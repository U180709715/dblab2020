# 1. Show the films whose budget is greater than 10 million$ and ranking is less than 6.
### select * from movies where budget > 10000000 and ranking < 6;
# 2. Show the action films whose rating is greater than 8.8 and produced after 2009.
### select * from movies  INNER JOIN genres ON genres.movie_id=movies.movie_id WHERE genre_name="Action" AND rating > 8.8 AND year > 2009
# 3. Show the drama films whose duration is more than 150 minutes and oscars is more than 2.
### select title from movies  INNER JOIN genres ON genres.movie_id=movies.movie_id WHERE genre_name="Drama" and oscars>2 and duration > 150
# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
### select title from movies where movies.movie_id in (select * from  (select movie_id FROM movie_stars where star_id in (select star_id from stars where star_name="Orlando Bloom" or star_name="Ian McKellen")) p  group by movie_id having count(*) = 2);
# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.	 

# 6. Show the thriller films whose budget is greater than 25 million$.	 

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
 
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	 

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.

# 10.Compute the average budget of the films directed by Peter Jackson.

# 11.Show the Francis Ford Coppola film that has the minimum budget.

# 12.Show the film that has the most vote and has been produced in USA..
