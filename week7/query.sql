# 5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.

SELECT title
FROM movies
WHERE votes > 500000
  AND year < 2000
  AND movie_id IN (SELECT movie_id FROM movie_directors WHERE director_id IN (SELECT director_id FROM directors WHERE director_name = "Quentin Tarantino"));